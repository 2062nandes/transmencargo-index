/* FORMULARIO DE CONTACTO VUEJS*/
new Vue({
	el: '#form',
	data:{
		formSubmitted: false,
		vue:{
			nombre: '',
			telefono:'',
			movil:'',
			ciudad:'',
			direccion:'',
			email:'',
			mensaje:'',
			envio: ''
		}
	},
	created: function(){
		// this.consultarMail();
	},
	methods: {
		// consultarMail: function(){
		// 	this.$http.get('mail.php').then(function(response){
		// 				this.vue.envio = response.data;
		// 				console.log(response);
		// 			}, function(){
		// 	});
		// },
		isFormValid: function(){
			return this.nombre != '';
		},
		submitForm:function(){
			if(!this.isFormValid()) return;
			this.formSubmitted = true;
			this.$http.post('mail.php',{ vue:this.vue }).then(function(response){
				// console.log(response);
			this.vue.envio = response.data;
			this.clearForm();
			}, function(){});
		},
		clearForm: function(){
			this.vue.nombre = '';
			this.vue.email = '';
			this.vue.telefono = '';
			this.vue.movil = '';
			this.vue.direccion = '';
			this.vue.ciudad = '';
			this.vue.mensaje = '';
			this.vue.formSubmitted = false;
		}
	}
});